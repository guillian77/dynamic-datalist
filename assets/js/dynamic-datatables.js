/**
 * Construct Table
 * @param {Array} fields Fieldsnames
 * @param {Array} data Data for fieldnames
 * @param {Array} actions Actions buttons
 */
let constructTable = function(fields, data, actions = 0) {
    var table = "";
    fieldsCound = fields.length;

    /**
     * Construct table header
     */
    table += '<thead class="thead-dark">';
        table += "<tr>";
        
            // Loop fields inside table header
            fields.forEach(function(field) {
                if (field['show']) {
                    table += '<th> ' + field['name'] + '</th>';
                }
            });

        table += "</tr>";
    table += "</thead>";

    /**
     * Construct table body
     */
    table += "<tbody>";

        data.forEach(function(row) {
            table += "<tr>";

                var i = 0;
                for(key in row) {
                    if (fields[i]['show'])
                    {
                        table += "<td>" + row[key] + "</td>"
                    }
                    i++;
                }

                // Data
                // row.forEach(function(value, index) {
                //     if (fields[index]['show'])
                //     {
                //         table += "<td>" + value + "</td>"
                //     }
                // })

                // Actions
                table += "<td>";
                    actions.forEach(action => {
                        // inject user ID into span
                        btnTargeted = [action.slice(0, 5), ' data-uid="' + row[0] + '" ', action.slice(5)].join('');
                        
                        table += btnTargeted;
                    });
                table += "</td>"
            table += "</tr>";
        });

    table += "</tbody>";

    return table;
}

/**
 * Construct config
 * @param {Array} fields 
 * @param {Array} order 
 * @param {String} name
 */
let constructConfig = function(fields, order = [0, "asc"], name = "Inconnu") {
    let config = {
        "stateSave": false,
        "order": [order],
        "pagingType": "simple_numbers",
        "searching": true,
        "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "Tous"]], 
        "language": {
            "info": "" + name + " _START_ à _END_ sur _TOTAL_ sélectionnées",
            "emptyTable": "Aucun " + name + "",
            "lengthMenu": "_MENU_ " + name + " par page",
            "search": "Rechercher : ",
            "zeroRecords": "Aucun résultat de recherche",
            "paginate": {
                "previous": "Précédent",
                "next": "Suivant"
            },
            "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            "sInfoEmpty":      "" + name + " 0 à 0 sur 0 sélectionnée",
        },
        "columns": [],
        'retrieve': true
    }

    // Edit fields settings
    if (fields)
    {
        fields.forEach(field => {
            if (field.show)
            {
                let jsonString = '{ "orderable": ' + field['orderable'] + ' }';
                config.columns.push( JSON.parse(jsonString) );
            }                        
        });
    }    

    return config;
}