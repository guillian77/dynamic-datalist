/**
 * Simulation of Data from BDD
 */
let data = [
    { id: 1, firstname: "Guillian", name: "Aufrère", discordID: "dekadmin#2935", mail: "guillian@mail.fr", role: "DWWM" },
    { id: 2, firstname: "Lucas", name: "Campillo", discordID: "lucas#2935", mail: "lucas@mail.fr", role: "DWWM" },
    { id: 3, firstname: "Younes", name: "Melheb", discordID: "younes#2935", mail: "younes@mail.fr", role: "DWWM" },
    { id: 4, firstname: "Damien", name: "Lhaurado", discordID: "damien#2935", mail: "damien@mail.fr", role: "DWWM" },
    { id: 5, firstname: "Claire", name: "Bories", discordID: "claire#2935", mail: "claire@mail.fr", role: "DWWM" },
    { id: 6, firstname: "Maxime", name: "Mailhe", discordID: "maxime#2935", mail: "maxime@mail.fr", role: "DWWM" },
];

/**
 * Start making datatable
 */
let fields = [
    { "name": "id", "orderable": false, "show": false},
    { "name": "Prénom", "orderable": true, "show": true},
    { "name": "Nom", "orderable": true, "show": true},
    { "name": "Discord", "orderable": true, "show": true},
    { "name": "Mail", "orderable": false, "show": false},
    { "name": "Rôle", "orderable": false, "show": false},
    { "name": "Action", "orderable": false, "show": true},
];

// /!\ actions should be surrounded by a span !
let actions = [
    '<span><i class="fas fa-user-edit"></i></span>',
    '<span><i class="far fa-trash-alt ml-3"></i></span>'
];

let configuration = constructConfig(fields, [1, "asc"], "Utilisateurs")

$(document).ready(function() {
    let table = constructTable(fields, data, actions);
    $('#ticket__table').html(table)
    $('#ticket__table').DataTable(configuration);
});
